package com.acmelabs.cqrseventsourcedddfpcart.schemas.cart

import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.EventEnvelope
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

/*
I would like to experiment with having all of this code be generated from a schema and
using some kind of tool to force that schema to be backward compatible / append only.
 */

data class CartEventEnvelope(
    val id: String,
    val aggregateId: AggregateId,
    val eventType: String,
    val event: CartEvent,
    val number: Int,

): EventEnvelope {
    companion object {
        fun deserialize(json: String): CartEventEnvelope =
            jacksonObjectMapper().readValue(json)
    }
    fun serialize(): String =
        jacksonObjectMapper()
            .writeValueAsString(this)
}

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
sealed class CartEvent {
    object Created: CartEvent()

    data class ItemAdded(
        val item: Item
    ): CartEvent()

    data class ItemRemoved(
        val item: Item
    ): CartEvent()
}

typealias ItemId = String
typealias Sku = String

data class Item(
    val id: ItemId,
    val sku: Sku,
    val price: Price,
)

data class Price(
    val amount: Int,
    val currency: String,
)
