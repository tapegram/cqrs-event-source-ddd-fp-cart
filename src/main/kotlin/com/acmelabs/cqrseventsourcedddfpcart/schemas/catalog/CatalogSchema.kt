package com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog

import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.EventEnvelope
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEventEnvelope
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

data class ProductEventEnvelope(
    val id: String,
    val aggregateId: AggregateId,
    val eventType: String,
    val event: ProductEvent,
    val number: Int,
    ): EventEnvelope {
    companion object {
        fun deserialize(json: String): ProductEventEnvelope =
            jacksonObjectMapper().readValue(json)
    }
    fun serialize(): String =
        jacksonObjectMapper()
            .writeValueAsString(this)
}

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
sealed class ProductEvent {
    data class Created(
        val isAvailable: IsAvailable,
    ): ProductEvent()

    data class VariantAdded(
        val variant: Variant,
    ) : ProductEvent()

    data class VariantRemoved(
        val variant: Variant,
    ) : ProductEvent()

    data class AvailabilityUpdated(
        val isAvailable: IsAvailable
    ): ProductEvent()

    data class NameUpdated(val name: Name): ProductEvent()
}

data class Variant(
    val id: VariantId,
    val sku: Sku,
    val prices: Prices,
)

typealias VariantId = String
typealias Sku = String
typealias IsAvailable = Boolean
typealias Name = String

/*
Oversimplification of a pricing system
 */
data class Prices(
    val usd: Price? = null,
    val cad: Price? = null,
    val gbp: Price? = null,
)

data class Price(
    val amount: Int,
)


