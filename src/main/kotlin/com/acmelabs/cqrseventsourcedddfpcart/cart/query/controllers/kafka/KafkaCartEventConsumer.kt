package com.acmelabs.cqrseventsourcedddfpcart.cart.query.controllers.kafka

import com.acmelabs.cqrseventsourcedddfpcart.cart.query.data.RemovedSkusRepo
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEventEnvelope
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class KafkaCartEventConsumer(
    private val removedSkusRepo: RemovedSkusRepo
) {
    private val logger = LoggerFactory.getLogger(javaClass)
    @KafkaListener(topics = ["CART_EVENTS"], groupId = "cart-event-consumer")
    fun processMessage(message: String) = runBlocking {
        logger.info("got message: {}", message)

        val eventEnvelope: CartEventEnvelope = CartEventEnvelope.deserialize(message)
        logger.info("deserialize $eventEnvelope")

        when (val event = eventEnvelope.event) {
            CartEvent.Created -> Unit
            is CartEvent.ItemAdded -> Unit
            is CartEvent.ItemRemoved -> removedSkusRepo.addSku(event.item.sku)
        }
    }
}
