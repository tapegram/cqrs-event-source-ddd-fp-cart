package com.acmelabs.cqrseventsourcedddfpcart.cart.query.controllers.json

import com.acmelabs.cqrseventsourcedddfpcart.cart.query.data.RemovedSkusRepo
import kotlinx.coroutines.runBlocking
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/query")
class ReadModelController(
    private val removedSkusRepo: RemovedSkusRepo
) {
    @GetMapping("/removed-skus")
    fun getRemovedSkus() = runBlocking {
        removedSkusRepo
            .getAll()
            .sortedBy { it.first }
    }
}
