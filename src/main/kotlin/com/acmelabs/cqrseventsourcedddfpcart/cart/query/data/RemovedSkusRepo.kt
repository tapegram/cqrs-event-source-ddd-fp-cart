package com.acmelabs.cqrseventsourcedddfpcart.cart.query.data

import arrow.core.extensions.list.foldable.toList
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Sku
import org.springframework.stereotype.Component

/*
Pretend this was in redis or something
 */
@Component
data class RemovedSkusRepo(
    val removedSkus: MutableList<Pair<Sku, Int>> = mutableListOf()
) {
    suspend fun addSku(sku: Sku) {
        val existingCount = removedSkus.find { it.first == sku } ?: Pair(sku, 0)
        removedSkus.removeIf { it.first == sku }
        removedSkus.add(Pair(sku, existingCount.second + 1))
    }

    suspend fun getAll() = removedSkus.toList()
}
