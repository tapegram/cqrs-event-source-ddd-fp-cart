package com.acmelabs.cqrseventsourcedddfpcart.cart.command.service

import arrow.core.Either
import arrow.core.computations.either
import arrow.generic.coproduct3.Coproduct3
import arrow.generic.coproduct3.first
import arrow.generic.coproduct3.second
import arrow.generic.coproduct3.third
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.Command
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.Failure
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.decide
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.apply
import com.acmelabs.cqrseventsourcedddfpcart.common.FindByIdFailure
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Item
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.ItemId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Sku


typealias AddItemFailure = Coproduct3<Failure, FindByIdFailure, FindBySkuFailure>

suspend fun addItem(
    eventStore: CartEventStore,
    productRepo: ProductRepo,
    id: String,
    itemId: ItemId,
    sku: Sku,
): Either<AddItemFailure, CartResponse> = either {
    val (cart, version) = !eventStore.findCartById(id)
        .mapLeft<AddItemFailure> { it.second() }

    val product = !productRepo.findBySku(sku)
        .mapLeft<AddItemFailure> { FindBySkuFailure.Unknown.third() }

    val variant = product.flatMap { it.findVariantBySku(sku) }

    val events = !decide(
        cart,
        Command.AddItem(
            itemId = itemId,
            variant = variant,
        )
    )
        .mapLeft<AddItemFailure> { it.first() }

    eventStore.append(id, version, events)

    apply(cart, events).toResponse()
}
