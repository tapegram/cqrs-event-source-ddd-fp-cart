package com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain

import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEvent


fun apply(cart: Cart, events: List<CartEvent>): Cart =
    events.fold(cart) { state, event -> apply(state, event) }

private fun apply(cart: Cart, event: CartEvent): Cart =
    when (cart) {
        is Cart.Initial -> Cart.Open(id = cart.id)

        is Cart.Open -> when (event) {
            is CartEvent.Created -> cart
            is CartEvent.ItemAdded -> cart.copy(
                items=cart.items + event.item
            )
            is CartEvent.ItemRemoved -> cart.copy(
                items=cart.items.filter { it.id != event.item.id }
            )
        }
    }
