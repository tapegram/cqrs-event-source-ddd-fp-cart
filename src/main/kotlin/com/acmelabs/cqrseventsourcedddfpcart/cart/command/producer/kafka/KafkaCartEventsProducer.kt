package com.acmelabs.cqrseventsourcedddfpcart.cart.command.producer.kafka

import arrow.core.Either
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.common.EventBus
import com.acmelabs.cqrseventsourcedddfpcart.common.PublishFailure
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEventEnvelope
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

@Component
class KafkaCartEventsProducer(
    private val kafkaTemplate: KafkaTemplate<String, String>
): EventBus<CartEventEnvelope> {
    fun send(message: String) {
        kafkaTemplate.send("CART_EVENTS", message)
    }

    override suspend fun publish(id: AggregateId, events: List<CartEventEnvelope>): Either<PublishFailure, Unit> =
        Either.catch {
            events.forEach {
                kafkaTemplate.send(
                    "CART_EVENTS",
                    it.serialize()
                )
            }
        }
            .mapLeft { PublishFailure.Unknown }

}
