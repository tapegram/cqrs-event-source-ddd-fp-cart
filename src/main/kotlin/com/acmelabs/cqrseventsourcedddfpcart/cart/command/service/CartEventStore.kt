package com.acmelabs.cqrseventsourcedddfpcart.cart.command.service

import com.acmelabs.cqrseventsourcedddfpcart.common.EventStore
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEventEnvelope

interface CartEventStore: EventStore<CartEvent, CartEventEnvelope>
