package com.acmelabs.cqrseventsourcedddfpcart.cart.command.controllers.kafka.handlers

import arrow.core.Either
import arrow.core.computations.either
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.ProductRepo
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.Variant
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.addVariant
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Price
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEvent

sealed class HandleVariantAddedFailure {
    object Unknown : HandleVariantAddedFailure()
    object ProductDoesNotExist : HandleVariantAddedFailure()
    object CouldNotUpdateProduct: HandleVariantAddedFailure()
}

suspend fun handleVariantAdded(
    productRepo: ProductRepo,
    productId: AggregateId,
    event: ProductEvent.VariantAdded
): Either<HandleVariantAddedFailure, Unit> = either {
    val result = !productRepo.findById(productId)
        .mapLeft { HandleVariantAddedFailure.Unknown }
    val product = !result.toEither { HandleVariantAddedFailure.ProductDoesNotExist }

    !productRepo.upsert(
        product.addVariant(
            Variant(
                id = event.variant.id,
                sku = event.variant.sku,
                price = Price(amount = event.variant.prices.usd!!.amount, currency = "USD"),
                isAvailable = product.isAvailable,
            )
        )
    )
        .mapLeft { HandleVariantAddedFailure.CouldNotUpdateProduct }
}
