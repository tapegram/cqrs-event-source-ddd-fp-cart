package com.acmelabs.cqrseventsourcedddfpcart.cart.command.data

import arrow.core.Either
import arrow.core.Option
import arrow.core.right
import arrow.core.toOption
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.FindByIdFailure
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.FindBySkuFailure
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.Product
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.ProductId
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.UpsertProductFailure
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.ProductRepo
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Sku
import org.springframework.stereotype.Component

@Component
data class InMemoryProductRepo(val products: MutableList<Product> = mutableListOf()): ProductRepo {
    override suspend fun findById(id: ProductId): Either<FindByIdFailure, Option<Product>> =
        products
            .find { it.id == id }
            .toOption()
            .right()

    override suspend fun findBySku(sku: Sku): Either<FindBySkuFailure, Option<Product>> =
        products
            .find { it.variants.any { variant -> variant.sku == sku }}
            .toOption()
            .right()

    override suspend fun upsert(product: Product): Either<UpsertProductFailure, Unit> {
        products.removeAll { it.id == product.id }
        products.add(product)
        return Unit.right()
    }

}
