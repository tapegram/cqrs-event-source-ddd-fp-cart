package com.acmelabs.cqrseventsourcedddfpcart.cart.command.service

import arrow.core.Either
import arrow.core.computations.either
import arrow.generic.coproduct2.Coproduct2
import arrow.generic.coproduct2.first
import arrow.generic.coproduct2.second
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.Command
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.Failure
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.apply
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.decide
import com.acmelabs.cqrseventsourcedddfpcart.common.FindByIdFailure
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.ItemId

typealias RemoveItemFailure = Coproduct2<Failure, FindByIdFailure>

suspend fun removeItem(
    eventStore: CartEventStore,
    id: String,
    itemId: ItemId,
): Either<RemoveItemFailure, CartResponse> = either {
    val (cart, version) = !eventStore.findCartById(id)
        .mapLeft<RemoveItemFailure> { it.second() }

    val events = !decide(cart, Command.RemoveItem(itemId = itemId))
        .mapLeft<RemoveItemFailure> { it.first() }

    eventStore.append(id, version, events)

    apply(cart, events)
        .toResponse()
}
