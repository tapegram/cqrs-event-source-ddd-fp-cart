package com.acmelabs.cqrseventsourcedddfpcart.cart.command.controllers.json

import com.acmelabs.cqrseventsourcedddfpcart.cart.command.data.EventStoreCartEventStore
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.producer.inmemory.NoOpCartEventBus
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.CartEventStore
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.ProductRepo
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.addItem
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.createCart
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.removeItem
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.ItemId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Sku
import kotlinx.coroutines.runBlocking
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

data class ItemRequest(
    val sku: String,
)

data class PriceRequest(
    val amount: Int,
    val currency: String,
)

@RestController
@RequestMapping("/api")
class CartController(
    private val eventStore: CartEventStore,
    private val productRepo: ProductRepo,
) {
    suspend fun _createCart(id: AggregateId) =
        createCart(
            eventStore = eventStore,
            id = id
        ).fold(
            { it.toString() },
            { it }
        )

    suspend fun _addItem(id: AggregateId, itemId: ItemId, sku: Sku) =
        addItem(
            eventStore = eventStore,
            productRepo = productRepo,
            id = id,
            itemId = itemId,
            sku = sku,
        ).fold(
            { it.toString() },
            { it }
        )

    suspend fun _removeItem(id: AggregateId, itemId: ItemId) =
        removeItem(
            eventStore = eventStore,
            id = id,
            itemId = itemId,
        ).fold(
            { it.toString() },
            { it }
        )

    @PostMapping("/cart")
    fun create() = runBlocking { _createCart(UUID.randomUUID().toString()) }

    @PostMapping("/cart/{id}/item")
    fun createItem(
        @PathVariable id: String,
        @RequestBody request: ItemRequest,
    ) = runBlocking {
        _addItem(
            id = id,
            itemId = UUID.randomUUID().toString(),
            sku = request.sku,
        )
    }

    @DeleteMapping("/cart/{id}/item/{itemId}")
    fun destroyItem(
        @PathVariable id: String,
        @PathVariable itemId: String,
    ) = runBlocking {
        _removeItem(
            id = id,
            itemId = itemId,
        )
    }

    @PostMapping("/cart/eventStore")
    fun tryEventStore() = runBlocking {
        EventStoreCartEventStore(NoOpCartEventBus).findById("123")
    }
}