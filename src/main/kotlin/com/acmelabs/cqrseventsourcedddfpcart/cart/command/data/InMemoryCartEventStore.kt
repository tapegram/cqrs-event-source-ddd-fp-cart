package com.acmelabs.cqrseventsourcedddfpcart.cart.command.data

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.acmelabs.cqrseventsourcedddfpcart.common.EventBus
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.CartEventStore
import com.acmelabs.cqrseventsourcedddfpcart.common.FindByIdFailure
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEventEnvelope
import java.util.UUID

data class InMemoryCartEventStore(
    override val eventBus: EventBus<CartEventEnvelope>,
    val store: MutableList<CartEventEnvelope> = mutableListOf()
): CartEventStore {
    override suspend fun findById(id: String): Either<FindByIdFailure, List<CartEvent>> {
        val events = store.filter { it.aggregateId == id }
        return if (events.isEmpty()) {
            FindByIdFailure.AggregateDoesNotExist.left()
        } else {
            events
                .sortedBy { it.number }
                .map { it.event }
                .right()
        }
    }

    override suspend fun append(
        id: String,
        previousVersion: Int,
        events: List<CartEvent>
    ): List<CartEventEnvelope> {
        val eventEnvelopes = events.mapIndexed { index, event ->
            CartEventEnvelope(
                id = UUID.randomUUID().toString(),
                aggregateId = id,
                eventType = event::class.java.name,
                event = event,
                number = previousVersion + index
            )
        }
        store.addAll(eventEnvelopes)
        eventBus.publish(id, eventEnvelopes)
        return eventEnvelopes
    }
}
