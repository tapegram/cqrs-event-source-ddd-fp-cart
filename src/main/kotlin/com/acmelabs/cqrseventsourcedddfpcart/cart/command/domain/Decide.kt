package com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain

import arrow.core.Either
import arrow.core.Option
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.Variant
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Item
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.ItemId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Sku

sealed class Failure {
    object CartMustBeCreatedBeforeItCanBeUpdated : Failure()
    object CanNotRecreateAnExistingCart : Failure()
    object SkuDoesNotExist : Failure()
    data class SkuIsNotAvailable(val sku: Sku) : Failure()
}

fun decide(cart: Cart, command: Command): Either<Failure, List<CartEvent>> =
    when (cart) {
        is Cart.Initial -> decide(cart, command)
        is Cart.Open -> decide(cart, command)
    }

private fun decide(cart: Cart.Initial, command: Command): Either<Failure, List<CartEvent>> =
    when (command) {
        is Command.Create -> listOf(CartEvent.Created).right()
        else -> Failure.CartMustBeCreatedBeforeItCanBeUpdated.left()
    }

private fun decide(cart: Cart.Open, command: Command): Either<Failure, List<CartEvent>> =
    when (command) {
        is Command.Create -> Failure.CanNotRecreateAnExistingCart.left()

        is Command.AddItem -> addItem(cart, command.itemId, command.variant)

        is Command.RemoveItem -> removeItem(cart, command.itemId).right()
    }

private fun addItem(cart: Cart.Open, itemId: ItemId, variant: Option<Variant>): Either<Failure, List<CartEvent>> =
    variant
        .toEither { Failure.SkuDoesNotExist }
        .flatMap { variant ->
            when (variant.isAvailable) {
                true -> listOf(CartEvent.ItemAdded(
                    item = Item(
                        id = itemId,
                        sku = variant.sku,
                        price = variant.price
                    )
                )).right()

                false -> Failure.SkuIsNotAvailable(variant.sku).left()
            }
        }

private fun removeItem(cart: Cart.Open, itemId: ItemId): List<CartEvent> =
    when (val item = cart.items.find { it.id == itemId }) {
        null -> emptyList()
        else -> listOf(
            CartEvent.ItemRemoved(
                item = item
            )
        )
    }
