package com.acmelabs.cqrseventsourcedddfpcart.cart.command.data

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.CartEventStore
import com.acmelabs.cqrseventsourcedddfpcart.common.EventBus
import com.acmelabs.cqrseventsourcedddfpcart.common.FindByIdFailure
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEventEnvelope
import com.eventstore.dbclient.EventData
import com.eventstore.dbclient.EventStoreDBClient
import com.eventstore.dbclient.EventStoreDBConnectionString
import com.eventstore.dbclient.ReadStreamOptions
import org.springframework.stereotype.Component
import java.util.UUID

@Component
data class EventStoreCartEventStore(
    override val eventBus: EventBus<CartEventEnvelope>,
    private val client: EventStoreDBClient = EventStoreDBClient.create(
        EventStoreDBConnectionString.parseOrThrow(
            "esdb://localhost:2113?tls=false&keepAliveInterval=-1&keepAliveTimeout=-1"
        )
    )
): CartEventStore {
    override suspend fun findById(id: String): Either<FindByIdFailure, List<CartEvent>> {
        val events = client
            .readStream(
                "carts-$id",
                ReadStreamOptions.get().fromStart().notResolveLinkTos()
            )
            .get()
            .events
            .map { CartEventEnvelope.deserialize(it.originalEvent.eventData.decodeToString()) }

        return if (events.isEmpty()) {
            FindByIdFailure.AggregateDoesNotExist.left()
        } else {
            events
                .sortedBy { it.number }
                .map { it.event }
                .right()
        }
    }

    override suspend fun append(id: String, previousVersion: Int, events: List<CartEvent>): List<CartEventEnvelope> = events
        .mapIndexed { index, event ->
            CartEventEnvelope(
                id = UUID.randomUUID().toString(),
                aggregateId = id,
                eventType = event::class.java.name,
                event = event,
                number = previousVersion + index
            )
        }.also {
            client.appendToStream(
                "carts-$id",
                *it.map { event ->
                    EventData.builderAsJson(event.eventType, event).build()
                }.toTypedArray()
            )
            eventBus.publish(id, it)
        }
}
