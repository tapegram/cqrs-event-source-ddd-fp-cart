package com.acmelabs.cqrseventsourcedddfpcart.cart.command.controllers.kafka.handlers

import arrow.core.Either
import arrow.core.computations.either
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.ProductRepo
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.setUnavailable
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId


sealed class HandleMadeUnavailable {
    object Unknown : HandleMadeUnavailable()
    object ProductDoesNotExist : HandleMadeUnavailable()
    object CouldNotUpdateProduct: HandleMadeUnavailable()
}

suspend fun handleMadeUnavailable(
    productRepo: ProductRepo,
    productId: AggregateId,
): Either<HandleMadeUnavailable, Unit> = either {
    val result = !productRepo.findById(productId)
        .mapLeft { HandleMadeUnavailable.Unknown }
    val product = !result.toEither { HandleMadeUnavailable.ProductDoesNotExist }

    !productRepo.upsert(
        product.setUnavailable()
    )
        .mapLeft { HandleMadeUnavailable.CouldNotUpdateProduct }
}
