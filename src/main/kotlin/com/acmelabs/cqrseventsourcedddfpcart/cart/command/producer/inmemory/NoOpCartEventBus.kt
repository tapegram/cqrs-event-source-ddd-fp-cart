package com.acmelabs.cqrseventsourcedddfpcart.cart.command.producer.inmemory

import arrow.core.Either
import arrow.core.right
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.common.EventBus
import com.acmelabs.cqrseventsourcedddfpcart.common.PublishFailure
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEventEnvelope

object NoOpCartEventBus: EventBus<CartEventEnvelope> {
    override suspend fun publish(id: AggregateId, events: List<CartEventEnvelope>): Either<PublishFailure, Unit> = Unit.right()
}