package com.acmelabs.cqrseventsourcedddfpcart.cart.command.service

import arrow.core.Either
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.Cart
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.apply
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.common.FindByIdFailure

typealias Version = Int

suspend fun CartEventStore.findCartById(id: AggregateId): Either<FindByIdFailure, Pair<Cart, Version>> =
    this
        .findById(id)
        .map { events ->
            Pair(apply(Cart.Initial(id), events), events.count())
        }
