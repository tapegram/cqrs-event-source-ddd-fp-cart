package com.acmelabs.cqrseventsourcedddfpcart.cart.command.controllers.kafka.handlers

import arrow.core.Either
import arrow.core.computations.either
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.ProductRepo
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.setAvailable
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId

sealed class HandleMadeAvailable {
    object Unknown : HandleMadeAvailable()
    object ProductDoesNotExist : HandleMadeAvailable()
    object CouldNotUpdateProduct: HandleMadeAvailable()
}

suspend fun handleMadeAvailable(
    productRepo: ProductRepo,
    productId: AggregateId,
): Either<HandleMadeAvailable, Unit> = either {
    val result = !productRepo.findById(productId)
        .mapLeft { HandleMadeAvailable.Unknown }
    val product = !result.toEither { HandleMadeAvailable.ProductDoesNotExist }

    !productRepo.upsert(
        product.setAvailable()
    )
        .mapLeft { HandleMadeAvailable.CouldNotUpdateProduct }
}
