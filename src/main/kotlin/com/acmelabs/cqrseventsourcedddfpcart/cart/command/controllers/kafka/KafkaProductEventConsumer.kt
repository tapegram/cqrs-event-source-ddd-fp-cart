package com.acmelabs.cqrseventsourcedddfpcart.cart.command.controllers.kafka

import com.acmelabs.cqrseventsourcedddfpcart.cart.command.controllers.kafka.handlers.handleMadeAvailable
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.controllers.kafka.handlers.handleMadeUnavailable
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.controllers.kafka.handlers.handleVariantAdded
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.controllers.kafka.handlers.handleVariantRemoved
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.Product
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.ProductRepo
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEventEnvelope
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
data class KafkaProductEventConsumer(
    val productRepo: ProductRepo
) {
    private val logger = LoggerFactory.getLogger(javaClass)
    @KafkaListener(topics = ["PRODUCT_EVENTS"], groupId = "product-event-consumer")
    fun processMessage(message: String): Unit = runBlocking {
        logger.info("got message: {}", message)

        val eventEnvelope: ProductEventEnvelope = ProductEventEnvelope.deserialize(message)
        logger.info("deserialize $eventEnvelope")

        when (val event = eventEnvelope.event) {
            is ProductEvent.Created -> productRepo.upsert(
                Product(
                    id = eventEnvelope.aggregateId,
                    isAvailable = event.isAvailable,
                    variants = emptyList()
                )
            )
            is ProductEvent.VariantAdded -> handleVariantAdded(productRepo, eventEnvelope.aggregateId, event)
            is ProductEvent.VariantRemoved -> handleVariantRemoved(productRepo, eventEnvelope.aggregateId, event)
            is ProductEvent.AvailabilityUpdated -> when (event.isAvailable) {
                true -> handleMadeAvailable(productRepo, eventEnvelope.aggregateId)
                false -> handleMadeUnavailable(productRepo, eventEnvelope.aggregateId)
            }
            is ProductEvent.NameUpdated -> Unit
        }
    }
}
