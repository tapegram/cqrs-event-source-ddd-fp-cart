package com.acmelabs.cqrseventsourcedddfpcart.cart.command.service

import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.Cart
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Item

data class CartResponse(
    val id: String,
    val items: List<Item>
)

fun Cart.toResponse(): CartResponse = when (this) {
    is Cart.Initial -> CartResponse(
        id = this.id,
        items = emptyList()
    )
    is Cart.Open -> CartResponse(
        id = this.id,
        items = this.items
    )
}
