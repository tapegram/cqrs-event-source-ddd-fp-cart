package com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain

import arrow.core.Option
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.Variant
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.ItemId

sealed class Command {
    object Create : Command()

    data class AddItem(
        val itemId: ItemId,
        val variant: Option<Variant>,
    ) : Command()

    data class RemoveItem(
        val itemId: ItemId,
    ) : Command()
}
