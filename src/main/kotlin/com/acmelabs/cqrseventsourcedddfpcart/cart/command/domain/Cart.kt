package com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain

import com.acmelabs.cqrseventsourcedddfpcart.common.Aggregate
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Item

sealed class Cart: Aggregate {
    data class Initial(
        override val id: AggregateId,
    ) : Cart()

    data class Open(
        override val id: AggregateId,
        val items: List<Item> = emptyList()
    ) : Cart()
}
