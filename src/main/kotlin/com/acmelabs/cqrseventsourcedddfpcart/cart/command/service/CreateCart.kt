package com.acmelabs.cqrseventsourcedddfpcart.cart.command.service

import arrow.core.Either
import arrow.core.computations.either
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.Cart
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.Command
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.Failure
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.decide

typealias CreateCartFailure = Failure

suspend fun createCart(
    eventStore: CartEventStore,
    id: String
): Either<CreateCartFailure, CartResponse> = either {
    val events = !decide(Cart.Initial(id), Command.Create)
    val cart = com.acmelabs.cqrseventsourcedddfpcart.cart.command.domain.apply(Cart.Initial(id), events)

    eventStore.append(id, 0, events)

    cart.toResponse()
}
