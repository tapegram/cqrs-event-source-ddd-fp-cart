package com.acmelabs.cqrseventsourcedddfpcart.cart.command.service

import arrow.core.Either
import arrow.core.Option
import arrow.core.toOption
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Price
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Sku

interface ProductRepo{
    suspend fun findById(id: ProductId): Either<FindByIdFailure, Option<Product>>
    suspend fun findBySku(sku: Sku): Either<FindBySkuFailure, Option<Product>>
    suspend fun upsert(product: Product): Either<UpsertProductFailure, Unit>
}

sealed class FindByIdFailure {
    object Unknown : FindByIdFailure()
}
sealed class FindBySkuFailure {
    object Unknown : FindBySkuFailure()
}
sealed class UpsertProductFailure {
    object Unknown : UpsertProductFailure()
}

typealias VariantId = String
typealias ProductId = String
typealias IsAvailable = Boolean

data class Product(
    val id: ProductId,
    val isAvailable: IsAvailable,
    val variants: List<Variant>,
)

fun Product.setAvailable(): Product =
    this.copy(
        isAvailable = true,
        variants = variants.map { it.setAvailable() }
    )

fun Product.setUnavailable(): Product =
    this.copy(
        isAvailable = false,
        variants = variants.map { it.setUnavailable() }
    )

fun Product.findVariantBySku(sku: Sku): Option<Variant> =
    variants
        .find { it.sku == sku }
        .toOption()

fun Product.addVariant(variant: Variant): Product =
    this.copy(
        variants = this.variants + variant
    )

fun Product.removeVariant(variantId: VariantId): Product =
    this.copy(
        variants = this.variants.filterNot {  it.id == variantId }
    )

data class Variant(
    val id: VariantId,
    val sku: Sku,
    val price: Price,
    val isAvailable: IsAvailable,
)

fun Variant.setAvailable(): Variant =
    this.copy(
        isAvailable = true
    )

fun Variant.setUnavailable(): Variant =
    this.copy(
        isAvailable = false
    )
