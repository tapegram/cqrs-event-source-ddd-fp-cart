package com.acmelabs.cqrseventsourcedddfpcart.cart.command.controllers.kafka.handlers

import arrow.core.Either
import arrow.core.computations.either
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.ProductRepo
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.removeVariant
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEvent

sealed class HandleVariantRemovedFailure {
    object Unknown : HandleVariantRemovedFailure()
    object ProductDoesNotExist : HandleVariantRemovedFailure()
    object CouldNotUpdateProduct: HandleVariantRemovedFailure()
}

suspend fun handleVariantRemoved(
    productRepo: ProductRepo,
    productId: AggregateId,
    event: ProductEvent.VariantRemoved,
): Either<HandleVariantRemovedFailure, Unit> = either {
    val result = !productRepo.findById(productId)
        .mapLeft { HandleVariantRemovedFailure.Unknown }
    val product = !result.toEither { HandleVariantRemovedFailure.ProductDoesNotExist }

    !productRepo.upsert(
        product.removeVariant(event.variant.id)
    )
        .mapLeft { HandleVariantRemovedFailure.CouldNotUpdateProduct }
}
