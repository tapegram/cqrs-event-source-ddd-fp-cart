package com.acmelabs.cqrseventsourcedddfpcart

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CqrsEventSourceDddFpCartApplication

fun main(args: Array<String>) {
	runApplication<CqrsEventSourceDddFpCartApplication>(*args)
}
