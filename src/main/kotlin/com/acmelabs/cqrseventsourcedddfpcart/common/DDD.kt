package com.acmelabs.cqrseventsourcedddfpcart.common

interface Aggregate {
    val id: AggregateId
}

typealias AggregateId = String

