package com.acmelabs.cqrseventsourcedddfpcart.common

import arrow.core.Either
import com.acmelabs.cqrseventsourcedddfpcart.schemas.EventEnvelope
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEventEnvelope

sealed class FindByIdFailure {
    object AggregateDoesNotExist : FindByIdFailure()
}

interface EventStore<A, B : EventEnvelope> {
    val eventBus: EventBus<B>
    suspend fun findById(id: String): Either<FindByIdFailure, List<A>>

    /*
    TODO: Make this an either return type and bubble up publishing failures
     */
    suspend fun append(
        id: String,
        previousVersion: Int,
        events: List<A>
    ): List<B>
}