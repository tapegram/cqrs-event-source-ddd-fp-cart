package com.acmelabs.cqrseventsourcedddfpcart.common

import arrow.core.Either
import com.acmelabs.cqrseventsourcedddfpcart.schemas.EventEnvelope

sealed class PublishFailure {
    object Unknown : PublishFailure()
}
interface EventBus<E : EventEnvelope> {
    suspend fun publish(id: AggregateId, events: List<E>): Either<PublishFailure, Unit>
}
