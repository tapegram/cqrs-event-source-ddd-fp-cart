package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.producer.inmemory

import arrow.core.Either
import arrow.core.right
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.common.EventBus
import com.acmelabs.cqrseventsourcedddfpcart.common.PublishFailure
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEventEnvelope

object NoOpProductEventBus: EventBus<ProductEventEnvelope> {
    override suspend fun publish(id: AggregateId, events: List<ProductEventEnvelope>): Either<PublishFailure, Unit> = Unit.right()
}