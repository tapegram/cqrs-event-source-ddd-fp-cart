package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service

import arrow.core.Either
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.Product
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.apply
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.common.FindByIdFailure

typealias Version = Int

suspend fun ProductEventStore.findProductById(id: AggregateId): Either<FindByIdFailure, Pair<Product, Version>> =
    this
        .findById(id)
        .map { events ->
            Pair(apply(Product.Initial(id), events), events.count())
        }
