package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service

import arrow.core.Either
import arrow.core.computations.either
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.Command
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.Failure
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.Product
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.decide
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.apply
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId

typealias CreateProductFailure = Failure

suspend fun createProduct(
    eventStore: ProductEventStore,
    id: AggregateId,
): Either<CreateProductFailure, ProductResponse> = either {
    val events = !decide(Product.Initial(id), Command.Create)
    eventStore.append(id, 0, events)

    apply(Product.Initial(id), events)
        .toResponse()
}
