package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service

import arrow.core.Either
import arrow.core.computations.either
import arrow.generic.coproduct2.Coproduct2
import arrow.generic.coproduct2.first
import arrow.generic.coproduct2.second
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.Command
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.Failure
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.decide
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.common.FindByIdFailure
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Variant
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.apply

typealias SetAvailableFailure = Coproduct2<Failure, FindByIdFailure>

suspend fun setAvailable(
    eventStore: ProductEventStore,
    id: AggregateId,
    isAvailable: Boolean,
): Either<SetAvailableFailure, ProductResponse> = either {
    val (product, version) = !eventStore.findProductById(id)
        .mapLeft<AddVariantFailure> { it.second() }

    val events = !decide(product, Command.SetAvailable(isAvailable = isAvailable))
        .mapLeft<AddVariantFailure> { it.first() }

    eventStore.append(id, version, events)
    apply(product, events).toResponse()
}

