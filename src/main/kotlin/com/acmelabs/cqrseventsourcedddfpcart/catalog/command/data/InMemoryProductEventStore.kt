package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.data

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.acmelabs.cqrseventsourcedddfpcart.common.EventBus
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.service.CartEventStore
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service.ProductEventStore
import com.acmelabs.cqrseventsourcedddfpcart.common.FindByIdFailure
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.CartEventEnvelope
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEventEnvelope
import org.springframework.stereotype.Component
import java.util.UUID

@Component
data class InMemoryProductEventStore(
    override val eventBus: EventBus<ProductEventEnvelope>,
    val store: MutableList<ProductEventEnvelope> = mutableListOf()
): ProductEventStore {
    override suspend fun findById(id: String): Either<FindByIdFailure, List<ProductEvent>> {
        val events = store.filter { it.aggregateId == id }
        return if (events.isEmpty()) {
            FindByIdFailure.AggregateDoesNotExist.left()
        } else {
            events
                .sortedBy { it.number }
                .map { it.event }
                .right()
        }
    }

    override suspend fun append(
        id: String,
        previousVersion: Int,
        events: List<ProductEvent>
    ): List<ProductEventEnvelope> {
        val eventEnvelopes = events.mapIndexed { index, event ->
            ProductEventEnvelope(
                id = UUID.randomUUID().toString(),
                aggregateId = id,
                eventType = event::class.java.name,
                event = event,
                number = previousVersion + index
            )
        }
        store.addAll(eventEnvelopes)
        eventBus.publish(id, eventEnvelopes)
        return eventEnvelopes
    }
}
