package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain

import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.IsAvailable
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Name
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Variant
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.VariantId

sealed class Command {
    object Create : Command()

    data class AddVariant(
        val variant: Variant
    ) : Command()

    data class RemoveVariant(
        val variantId: VariantId
    ) : Command()

    data class SetAvailable(
        val isAvailable: IsAvailable
    ) : Command()

    data class SetName(
        val name: Name
    ) : Command()
}