package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.IsAvailable
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Name
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Sku
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Variant
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.VariantId

sealed class Failure {
    object Unknown : Failure()
    data class SkuAlreadyExistsInProduct(val sku: Sku) : Failure()
    data class VariantNotFound(val id: VariantId) : Failure()
}

fun decide(product: Product, command: Command): Either<Failure, List<ProductEvent>> =
    when (product) {
        is Product.Initial -> decide(product, command)
        is Product.Configured -> decide(product, command)
    }

private fun decide(product: Product.Initial, command: Command): Either<Failure, List<ProductEvent>> =
    when (command) {
        is Command.Create -> listOf(
            ProductEvent.Created(
                isAvailable = false,
            )
        ).right()

        is Command.AddVariant -> TODO()
        is Command.RemoveVariant -> TODO()
        is Command.SetAvailable -> TODO()
        is Command.SetName -> TODO()
    }

private fun decide(product: Product.Configured, command: Command): Either<Failure, List<ProductEvent>> =
    when (command) {
        is Command.Create -> TODO()
        is Command.AddVariant -> addVariant(product, command.variant)
        is Command.RemoveVariant -> removeVariant(product, command.variantId)
        is Command.SetAvailable -> setAvailable(product, command.isAvailable).right()
        is Command.SetName -> setName(product, command.name).right()
    }

private fun setName(
    product: Product.Configured,
    name: Name,
): List<ProductEvent> =
    when (product.name == name) {
        true -> emptyList()
        false -> listOf(ProductEvent.NameUpdated(name = name))
    }

private fun setAvailable(
    product: Product.Configured,
    isAvailable: IsAvailable,
): List<ProductEvent> =
    when (product.isAvailable == isAvailable) {
        true -> emptyList()
        false -> listOf(ProductEvent.AvailabilityUpdated(isAvailable))
    }

private fun addVariant(
    product: Product.Configured,
    variant: Variant
): Either<Failure, List<ProductEvent>> =
    when (product.variants.find { it.sku == variant.sku}) {
        null -> listOf(ProductEvent.VariantAdded(variant = variant)).right()
        else -> Failure.SkuAlreadyExistsInProduct(variant.sku).left()
    }

private fun removeVariant(
    product: Product.Configured,
    variantId: VariantId,
): Either<Failure, List<ProductEvent>> =
    when (val variant = product.variants.find { it.id == variantId }) {
        null -> Failure.VariantNotFound(variantId).left()
        else -> listOf(ProductEvent.VariantRemoved(variant = variant)).right()
    }
