package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service

import com.acmelabs.cqrseventsourcedddfpcart.common.EventStore
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEventEnvelope

interface ProductEventStore: EventStore<ProductEvent, ProductEventEnvelope>