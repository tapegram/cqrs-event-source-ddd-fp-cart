package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain

import com.acmelabs.cqrseventsourcedddfpcart.common.Aggregate
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.IsAvailable
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Name
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Variant

sealed class Product: Aggregate {
    data class Initial(
        override val id: AggregateId,
    ) : Product()

    data class Configured(
        override val id: AggregateId,
        val name: Name,
        val isAvailable: IsAvailable,
        val variants: List<Variant> = emptyList()
    ) : Product()
}
