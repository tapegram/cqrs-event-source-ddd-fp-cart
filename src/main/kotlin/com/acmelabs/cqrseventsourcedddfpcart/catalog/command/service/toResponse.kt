package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service

import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.Product
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.IsAvailable
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Name
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Variant

data class ProductResponse(
    val id: AggregateId,
    val name: Name,
    val isAvailable: IsAvailable,
    val variants: List<Variant> = emptyList()
)

fun Product.toResponse(): ProductResponse =
    when (this) {
        is Product.Initial -> ProductResponse(
            id = this.id,
            name = "",
            isAvailable = false,
            variants = emptyList()
        )

        is Product.Configured -> ProductResponse(
            id = this.id,
            name = this.name,
            isAvailable = this.isAvailable,
            variants = this.variants,
        )
    }