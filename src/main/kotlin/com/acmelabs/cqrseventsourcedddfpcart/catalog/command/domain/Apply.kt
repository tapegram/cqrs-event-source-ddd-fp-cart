package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain

import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Name
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEvent
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Variant
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.VariantId

fun apply(product: Product, events: List<ProductEvent>): Product =
    events.fold(product) { state, event -> apply(state, event) }

private fun apply(product: Product, event: ProductEvent): Product =
    when (product) {
        is Product.Initial -> apply(product, event)
        is Product.Configured -> apply(product, event)
    }

private fun apply(product: Product.Initial, event: ProductEvent): Product.Configured =
    when (event) {
        is ProductEvent.Created -> Product.Configured(
            id = product.id,
            name = "",
            isAvailable = event.isAvailable,
            variants = emptyList(),
        )

        is ProductEvent.VariantAdded -> TODO()
        is ProductEvent.VariantRemoved -> TODO()
        is ProductEvent.NameUpdated -> TODO()
        is ProductEvent.AvailabilityUpdated -> TODO()
    }

private fun apply(product: Product.Configured, event: ProductEvent): Product.Configured =
    when (event) {
        is ProductEvent.Created -> TODO()
        is ProductEvent.VariantAdded -> product.addVariant(event.variant)
        is ProductEvent.VariantRemoved -> product.removeVariant(event.variant.id)
        is ProductEvent.NameUpdated -> product.setName(event.name)
        is ProductEvent.AvailabilityUpdated -> when (event.isAvailable) {
            true -> product.setAvailable()
            false -> product.setUnavailable()
        }
    }

private fun Product.Configured.setName(name: Name): Product.Configured =
    this.copy(name = name)

private fun Product.Configured.setAvailable(): Product.Configured =
    this.copy(isAvailable = true)

private fun Product.Configured.setUnavailable(): Product.Configured =
    this.copy(isAvailable = false)

private fun Product.Configured.addVariant(variant: Variant): Product.Configured =
    this.copy(variants = this.variants + variant)

private fun Product.Configured.removeVariant(variantId: VariantId): Product.Configured =
    this.copy(variants = this.variants.filterNot { it.id == variantId})
