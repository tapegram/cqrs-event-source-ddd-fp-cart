package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.controllers.json

import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service.ProductEventStore
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service.addVariant
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service.createProduct
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service.removeVariant
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service.setAvailable
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service.setName
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.IsAvailable
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Name
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Price
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Prices
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Sku
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Variant
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.VariantId
import kotlinx.coroutines.runBlocking
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.RuntimeException
import java.util.UUID

data class VariantRequest(
    val sku: Sku,
    val prices: Prices
)

data class ProductRequest(
    val name: Name,
    val isAvailable: IsAvailable,
)

data class TestProductRequest(
    val skus: List<Sku>
)

@RestController
@RequestMapping("/api/catalog/product")
class ProductController(
    private val eventStore: ProductEventStore,
) {
   suspend fun _createProduct(id: AggregateId) =
       createProduct(
           eventStore = eventStore,
           id = id,
       )

    suspend fun _addVariant(id: AggregateId, variant: Variant) =
        addVariant(
            eventStore = eventStore,
            id = id,
            variant = variant,
        )

    suspend fun _removeVariant(id: AggregateId, variantId: VariantId) =
        removeVariant(
            eventStore = eventStore,
            id = id,
            variantId = variantId,
        )

    suspend fun _setAvailable(id: AggregateId, isAvailable: IsAvailable) =
        setAvailable(
            eventStore = eventStore,
            id = id,
            isAvailable = isAvailable,
        )

    suspend fun _setName(id: AggregateId, name: Name) =
        setName(
            eventStore = eventStore,
            id = id,
            name = name,
        )

    @PostMapping("")
    fun create() = runBlocking {
        _createProduct(
            UUID.randomUUID().toString()
        ).fold(
            { throw RuntimeException(it::class.java.simpleName) },
            { it }
        )
    }

    @PostMapping("/{id}/variant")
    fun createVariant(
        @PathVariable id: String,
        @RequestBody request: VariantRequest,
    ) = runBlocking {
        _addVariant(
            id = id,
            variant = Variant(
                id = UUID.randomUUID().toString(),
                sku = request.sku,
                prices = request.prices,
            )
        ).fold(
            { throw RuntimeException(it::class.java.simpleName) },
            { it }
        )
    }

    @DeleteMapping("/{id}/variant/{variantId}")
    fun createVariant(
        @PathVariable id: String,
        @PathVariable variantId: String
    ) = runBlocking {
        _removeVariant(
            id = id,
            variantId = variantId,
        ).fold(
            { throw RuntimeException(it::class.java.simpleName) },
            { it }
        )
    }

    @PutMapping("/{id}")
    fun updateProduct(
        @PathVariable id: String,
        @RequestBody request: ProductRequest,
    ) = runBlocking {
        _setAvailable(id, request.isAvailable)
        _setName(id, request.name)
            .fold(
                { throw RuntimeException(it::class.java.simpleName) },
                { it }
            )

}

    @PostMapping("/test-product")
    fun createTestProduct(
        @RequestBody request: TestProductRequest,
    ) = runBlocking {
        val id = UUID.randomUUID().toString()
        _createProduct(id)
        val product = _setAvailable(id, true)

        request.skus.fold(product) { _, sku ->
            _addVariant(
                id = id,
                variant = Variant(
                    id = UUID.randomUUID().toString(),
                    sku = sku,
                    prices = Prices(usd = Price(100)),
                )
            )
        }.fold(
            { throw RuntimeException(it::class.java.simpleName) },
            { it }
        )
    }
}
