package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.producer.kafka

import arrow.core.Either
import arrow.core.right
import com.acmelabs.cqrseventsourcedddfpcart.common.AggregateId
import com.acmelabs.cqrseventsourcedddfpcart.common.EventBus
import com.acmelabs.cqrseventsourcedddfpcart.common.PublishFailure
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.ProductEventEnvelope
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

@Component
class KafkaProductEventProducer(
    private val kafkaTemplate: KafkaTemplate<String, String>
): EventBus<ProductEventEnvelope> {
    override suspend fun publish(
        id: AggregateId,
        events: List<ProductEventEnvelope>
    ): Either<PublishFailure, Unit> =
        Either.catch {
            events.forEach {
                kafkaTemplate.send(
                    "PRODUCT_EVENTS",
                    it.serialize()
                )
            }
        }
            .mapLeft { PublishFailure.Unknown }

}
