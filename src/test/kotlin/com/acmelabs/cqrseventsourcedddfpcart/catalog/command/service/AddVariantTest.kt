package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service

import arrow.generic.coproduct2.first
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.data.InMemoryProductEventStore
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.Failure
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.producer.inmemory.NoOpProductEventBus
import io.kotest.assertions.arrow.either.shouldBeLeft
import io.kotest.assertions.arrow.either.shouldBeRight
import io.kotest.core.spec.style.StringSpec
import io.kotest.property.Arb
import io.kotest.property.arbitrary.orNull
import io.kotest.property.arbitrary.uuid
import io.kotest.property.checkAll

class AddVariantTest : StringSpec({
    "add variant to empty product" {
        checkAll(Arb.uuid(), Arb.variant(), Arb.variant()) { id, variant, anotherVariant ->
            val eventStore = InMemoryProductEventStore(eventBus = NoOpProductEventBus)
            createProduct(
                eventStore = eventStore,
                id = id.toString(),
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = false,
                variants = emptyList()
            )

            addVariant(
                eventStore = eventStore,
                id = id.toString(),
                variant = variant
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = false,
                variants = listOf(variant)
            )

            if (variant.sku != anotherVariant.sku) {
                addVariant(
                    eventStore = eventStore,
                    id = id.toString(),
                    variant = anotherVariant
                ) shouldBeRight ProductResponse(
                    id = id.toString(),
                    name = "",
                    isAvailable = false,
                    variants = listOf(variant, anotherVariant)
                )
            } else {
                addVariant(
                    eventStore = eventStore,
                    id = id.toString(),
                    variant = anotherVariant
                ) shouldBeLeft Failure.SkuAlreadyExistsInProduct(sku = variant.sku).first()
            }
        }
    }
})