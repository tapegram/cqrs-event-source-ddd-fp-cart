package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service

import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Price
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Prices
import com.acmelabs.cqrseventsourcedddfpcart.schemas.catalog.Variant
import io.kotest.property.Arb
import io.kotest.property.arbitrary.bind
import io.kotest.property.arbitrary.orNull
import io.kotest.property.arbitrary.positiveInts
import io.kotest.property.arbitrary.string
import io.kotest.property.arbitrary.uuid

fun Arb.Companion.variant(): Arb<Variant> =
    Arb.bind(
        Arb.uuid(),
        Arb.string(),
        Arb.prices(),
    ) { id, sku, price ->
        Variant(
            id = id.toString(),
            sku = sku,
            prices = price,
        )
    }

fun Arb.Companion.prices(): Arb<Prices> =
    Arb.bind(
        Arb.positiveInts().orNull(),
        Arb.positiveInts().orNull(),
        Arb.positiveInts().orNull(),
    ) { usd, cad, gbp ->
        Prices(
            usd = usd?.let { Price(usd) },
            cad = cad?.let { Price(cad) },
            gbp = gbp?.let { Price(gbp) },
        )
    }

