package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service

import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.data.InMemoryProductEventStore
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.producer.inmemory.NoOpProductEventBus
import io.kotest.assertions.arrow.either.shouldBeRight
import io.kotest.core.spec.style.StringSpec
import io.kotest.property.Arb
import io.kotest.property.arbitrary.uuid
import io.kotest.property.checkAll

class CreateProductTest : StringSpec({
    "create a new product" {
        checkAll(Arb.uuid()) { id ->
            createProduct(
                eventStore = InMemoryProductEventStore(eventBus = NoOpProductEventBus),
                id = id.toString(),
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = false,
                variants = emptyList()
            )
        }
    }
})