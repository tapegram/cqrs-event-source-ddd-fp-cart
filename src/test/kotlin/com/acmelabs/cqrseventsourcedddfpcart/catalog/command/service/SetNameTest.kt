package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service

import arrow.generic.coproduct2.first
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.data.InMemoryProductEventStore
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.Failure
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.producer.inmemory.NoOpProductEventBus
import io.kotest.assertions.arrow.either.shouldBeLeft
import io.kotest.assertions.arrow.either.shouldBeRight
import io.kotest.core.spec.style.StringSpec
import io.kotest.property.Arb
import io.kotest.property.arbitrary.bool
import io.kotest.property.arbitrary.string
import io.kotest.property.arbitrary.uuid
import io.kotest.property.checkAll

class SetNameTest : StringSpec({
    "set name" {
        checkAll(Arb.uuid(), Arb.string(), Arb.string()) { id, name1, name2 ->
            val eventStore = InMemoryProductEventStore(eventBus = NoOpProductEventBus)
            createProduct(
                eventStore = eventStore,
                id = id.toString(),
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = false,
                variants = emptyList()
            )

            setName(
                eventStore = eventStore,
                id = id.toString(),
                name = name1
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = name1,
                isAvailable = false,
                variants = emptyList()
            )

            setName(
                eventStore = eventStore,
                id = id.toString(),
                name = name2
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = name2,
                isAvailable = false,
                variants = emptyList()
            )
        }
    }
})