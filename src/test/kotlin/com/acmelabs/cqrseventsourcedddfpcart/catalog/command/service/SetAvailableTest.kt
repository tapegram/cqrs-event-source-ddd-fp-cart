package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service

import arrow.generic.coproduct2.first
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.data.InMemoryProductEventStore
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.Failure
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.producer.inmemory.NoOpProductEventBus
import io.kotest.assertions.arrow.either.shouldBeLeft
import io.kotest.assertions.arrow.either.shouldBeRight
import io.kotest.core.spec.style.StringSpec
import io.kotest.property.Arb
import io.kotest.property.arbitrary.bool
import io.kotest.property.arbitrary.uuid
import io.kotest.property.checkAll

class SetAvailableTest : StringSpec({
    "set available" {
        checkAll(Arb.uuid(), Arb.bool(), Arb.bool()) { id, isAvailable1, isAvailable2 ->
            val eventStore = InMemoryProductEventStore(eventBus = NoOpProductEventBus)
            createProduct(
                eventStore = eventStore,
                id = id.toString(),
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = false,
                variants = emptyList()
            )

            setAvailable(
                eventStore = eventStore,
                id = id.toString(),
                isAvailable = isAvailable1
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = isAvailable1,
                variants = emptyList()
            )

            setAvailable(
                eventStore = eventStore,
                id = id.toString(),
                isAvailable = isAvailable2
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = isAvailable2,
                variants = emptyList()
            )
        }
    }
})