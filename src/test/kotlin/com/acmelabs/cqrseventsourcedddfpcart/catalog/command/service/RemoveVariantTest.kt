package com.acmelabs.cqrseventsourcedddfpcart.catalog.command.service

import arrow.generic.coproduct2.first
import arrow.generic.coproduct2.second
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.data.InMemoryProductEventStore
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.domain.Failure
import com.acmelabs.cqrseventsourcedddfpcart.catalog.command.producer.inmemory.NoOpProductEventBus
import com.acmelabs.cqrseventsourcedddfpcart.common.FindByIdFailure
import io.kotest.assertions.arrow.either.shouldBeLeft
import io.kotest.assertions.arrow.either.shouldBeRight
import io.kotest.core.spec.style.StringSpec
import io.kotest.property.Arb
import io.kotest.property.arbitrary.orNull
import io.kotest.property.arbitrary.uuid
import io.kotest.property.checkAll

class RemoveVariantTest : StringSpec({

    "non existent product" {
        checkAll(Arb.uuid(), Arb.variant()) { id, variant ->
            val eventStore = InMemoryProductEventStore(eventBus = NoOpProductEventBus)
            removeVariant(
                eventStore = eventStore,
                id = id.toString(),
                variantId = variant.id
            ) shouldBeLeft FindByIdFailure.AggregateDoesNotExist.second()
        }
    }
//
//            if (variant.sku != anotherVariant.sku) {
//                addVariant(
//                    eventStore = eventStore,
//                    id = id.toString(),
//                    variant = anotherVariant
//                ) shouldBeRight ProductResponse(
//                    id = id.toString(),
//                    name = "",
//                    isAvailable = false,
//                    variants = listOf(variant, anotherVariant)
//                )
//            } else {
//                addVariant(
//                    eventStore = eventStore,
//                    id = id.toString(),
//                    variant = anotherVariant
//                ) shouldBeLeft Failure.SkuAlreadyExistsInProduct(sku = variant.sku).first()
//            }
//        }

    "remove non existent variant" {
        checkAll(Arb.uuid(), Arb.variant()) { id, variant ->
            val eventStore = InMemoryProductEventStore(eventBus = NoOpProductEventBus)
            createProduct(
                eventStore = eventStore,
                id = id.toString(),
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = false,
                variants = emptyList()
            )

            removeVariant(
                eventStore = eventStore,
                id = id.toString(),
                variantId = variant.id
            ) shouldBeLeft Failure.VariantNotFound(id = variant.id).first()
        }
    }

    "remove variant" {
        checkAll(Arb.uuid(), Arb.variant(), Arb.variant()) { id, variant, otherVariant ->
            if (variant.id == otherVariant.id || variant.sku == otherVariant.sku) {
                return@checkAll
            }

            val eventStore = InMemoryProductEventStore(eventBus = NoOpProductEventBus)
            createProduct(
                eventStore = eventStore,
                id = id.toString(),
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = false,
                variants = emptyList()
            )

            addVariant(
                eventStore = eventStore,
                id = id.toString(),
                variant = variant
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = false,
                variants = listOf(variant)
            )

            addVariant(
                eventStore = eventStore,
                id = id.toString(),
                variant = otherVariant
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = false,
                variants = listOf(variant, otherVariant)
            )

            removeVariant(
                eventStore = eventStore,
                id = id.toString(),
                variantId = variant.id
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = false,
                variants = listOf(otherVariant)
            )

            removeVariant(
                eventStore = eventStore,
                id = id.toString(),
                variantId = otherVariant.id
            ) shouldBeRight ProductResponse(
                id = id.toString(),
                name = "",
                isAvailable = false,
                variants = emptyList()
            )
        }
    }
})