package com.acmelabs.cqrseventsourcedddfpcart.cart.command.service

import com.acmelabs.cqrseventsourcedddfpcart.cart.command.data.InMemoryCartEventStore
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.producer.inmemory.NoOpCartEventBus
import io.kotest.assertions.arrow.either.shouldBeRight
import io.kotest.core.spec.style.StringSpec
import io.kotest.property.checkAll

class CreateCartTest : StringSpec({
    "create a new cart" {
        checkAll<String> { id ->
            createCart(
                eventStore = InMemoryCartEventStore(eventBus = NoOpCartEventBus),
                id = id
            ) shouldBeRight CartResponse(
                id = id,
                items = emptyList()
            )
        }
    }
})
