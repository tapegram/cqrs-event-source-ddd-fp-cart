package com.acmelabs.cqrseventsourcedddfpcart.cart.command.service

import arrow.generic.coproduct3.second
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.data.InMemoryCartEventStore
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.data.InMemoryProductRepo
import com.acmelabs.cqrseventsourcedddfpcart.cart.command.producer.inmemory.NoOpCartEventBus
import com.acmelabs.cqrseventsourcedddfpcart.common.FindByIdFailure
import io.kotest.assertions.arrow.either.shouldBeLeft
import io.kotest.assertions.arrow.either.shouldBeRight
import io.kotest.core.spec.style.StringSpec
import io.kotest.property.Arb
import io.kotest.property.arbitrary.uuid
import io.kotest.property.checkAll
import java.util.UUID


class AddItemTest : StringSpec({
    "cart does not exist" {
        checkAll(Arb.uuid(), Arb.uuid(), itemArb) { id, otherId, item ->
            val eventStore = InMemoryCartEventStore(eventBus=NoOpCartEventBus)
            val variantRepo = InMemoryProductRepo(
                mutableListOf(
                    Product(
                        id = UUID.randomUUID().toString(),
                        isAvailable = true,
                        variants = listOf(
                            Variant(
                                id = UUID.randomUUID().toString(),
                                sku = item.sku,
                                isAvailable = true,
                                price = item.price
                            ),
                        )
                    )
                )
            )
            createCart(
                eventStore = eventStore,
                id = id.toString()
            ) shouldBeRight CartResponse(
                id = id.toString(),
                items = emptyList()
            )

            if (id != otherId) {
                addItem(
                    eventStore = eventStore,
                    productRepo = variantRepo,
                    id = otherId.toString(),
                    itemId = item.id,
                    sku = item.sku,
                ) shouldBeLeft FindByIdFailure.AggregateDoesNotExist.second()
            }
        }
    }

    "add an item to an empty cart" {
        checkAll(Arb.uuid(), itemArb) { id, item ->
            val eventStore = InMemoryCartEventStore(eventBus=NoOpCartEventBus)
            val variantRepo = InMemoryProductRepo(
                mutableListOf(
                    Product(
                        id = UUID.randomUUID().toString(),
                        isAvailable = true,
                        variants = listOf(
                            Variant(
                                id = UUID.randomUUID().toString(),
                                sku = item.sku,
                                isAvailable = true,
                                price = item.price
                            ),
                        )
                    )
                )
            )
            createCart(
                eventStore = eventStore,
                id = id.toString()
            ) shouldBeRight CartResponse(
                id = id.toString(),
                items = emptyList()
            )

            addItem(
                eventStore = eventStore,
                productRepo = variantRepo,
                id = id.toString(),
                itemId = item.id,
                sku = item.sku,
            ) shouldBeRight CartResponse(
                id = id.toString(),
                items = listOf(item)
            )
        }
    }

    "add an item to a cart with items" {
        checkAll(Arb.uuid(), itemArb, itemArb) { id, item, secondItem ->
            if (item.sku == secondItem.sku) return@checkAll

            val eventStore = InMemoryCartEventStore(eventBus=NoOpCartEventBus)
            val variantRepo = InMemoryProductRepo(
                mutableListOf(
                    Product(
                        id = UUID.randomUUID().toString(),
                        isAvailable = true,
                        variants = listOf(
                            Variant(
                                id = UUID.randomUUID().toString(),
                                sku = item.sku,
                                isAvailable = true,
                                price = item.price
                            ),
                            Variant(
                                id = UUID.randomUUID().toString(),
                                sku = secondItem.sku,
                                isAvailable = true,
                                price = secondItem.price
                            )
                        )
                    )
                )
            )
            createCart(
                eventStore = eventStore,
                id = id.toString()
            ) shouldBeRight CartResponse(
                id = id.toString(),
                items = emptyList()
            )

            addItem(
                eventStore = eventStore,
                productRepo = variantRepo,
                id = id.toString(),
                itemId = item.id,
                sku = item.sku,
            ) shouldBeRight CartResponse(
                id = id.toString(),
                items = listOf(item)
            )

            addItem(
                eventStore = eventStore,
                productRepo = variantRepo,
                id = id.toString(),
                itemId = secondItem.id,
                sku = secondItem.sku,
            ) shouldBeRight CartResponse(
                id = id.toString(),
                items = listOf(item, secondItem)
            )
        }
    }
})
