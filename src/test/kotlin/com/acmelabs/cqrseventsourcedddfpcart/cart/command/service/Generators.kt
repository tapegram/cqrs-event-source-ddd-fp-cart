package com.acmelabs.cqrseventsourcedddfpcart.cart.command.service

import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Item
import com.acmelabs.cqrseventsourcedddfpcart.schemas.cart.Price
import io.kotest.property.Arb
import io.kotest.property.Exhaustive
import io.kotest.property.arbitrary.bind
import io.kotest.property.arbitrary.int
import io.kotest.property.arbitrary.string
import io.kotest.property.arbitrary.uuid
import io.kotest.property.exhaustive.collection

val priceArb: Arb<Price> =
    Arb.bind(
        Arb.int(0, 999999999),
        Exhaustive.collection(listOf("USD", "CAD", "GBP", "EUR", "AUS"))
    ) { amount, currency ->
        Price(amount, currency)
    }

val itemArb: Arb<Item> =
    Arb.bind(
        Arb.uuid(),
        Arb.string(),
        priceArb
    ) { id, sku, price ->
        Item(
            id = id.toString(),
            sku = sku,
            price = price
        )
    }
