# Running dev
```bash
docker-compose up -d
./gradlew bootRun
```

When you are done
```bash
docker-compose down
```

# Catalog API
You can use `http://127.0.0.1:8080/api/catalog/product`

POST `http://127.0.0.1:8080/api/catalog/product` to create a product

PUT `http://127.0.0.1:8080/api/catalog/product` with a body of
```json
{
  "name": "new product name",
  "isAvailable": true
}
```
To update the name and available fields

You can POST to `http://127.0.0.1:8080/api/catalog/product/{id}/variant` with
```json
{
  "sku": "PL-01",
  "prices": {
    "USD": { "amount":  100},
    "CAD": { "amount":  100},
    "GBP": { "amount":  100}
  }
}
```

and DELETE `http://127.0.0.1:8080/api/catalog/product/{id}/variant/{variantId}` 

You can also use POST `http://127.0.0.1:8080/api/catalog/product/test-product`
```json
{
  "skus": [
    "PL-01",
    "PL-02" 
    // And so on
  ]
}
```
To create a default available product with variants for each sku (so you dont have to make a product, set it available, and create a bunch of variants one after another).

# Cart API
## Create a new cart 
POST to `http://127.0.0.1:8080/api/cart`
You should get back a response like
```json
{
    "id": "fe2fd342-5374-4d13-89d1-87cd1f2cb768",
    "items": []
}
```

## Add an item to an existing cart
POST to `http://127.0.0.1:8080/api/cart/{id}/item` with a request body like
```json
{
    "sku": "PL-01"
}
```
Where the sku is a real sku created via the product API (otherwise adding the item will not pass validation)

You should get back a response like
```json
{
    "id": "fe2fd342-5374-4d13-89d1-87cd1f2cb768",
    "items": [
        {
            "id": "3b4f1a04-9145-40cd-9e40-472f8e72d156",
            "sku": "PL-01",
            "price": {
                "amount": 200000,
                "currency": "USD"
            }
        }
    ]
}
```

## Remove an item from the cart
POST to `http://127.0.0.1:8080/api/cart/{id}/item/{itemId}`
If the item was in the cart it should be remove
```json

{
    "id": "fe2fd342-5374-4d13-89d1-87cd1f2cb768",
    "items": []
}
```

# Read model API
In the `query` half of this project there is a controller listening to the `CART_EVENTS` topic and using the events to update various read models (at the time of writing, there is one read model).

As you go about your day creating carts and adding and removing items, the query model is listening to those events and building its own models that can be served on demand.

## Removed Skus
Just to come up with an example, there is a query

GET `http://127.0.0.1:8080/api/query/removed-skus`

that returns a list of all the SKUs that have been removed from carts and how many times it has been removed.

This is a silly and easy example, but it is also something that would NOT be possible without a lot of extra auditing code in a traditional system. And it essentially comes for *free*.


# Schema and domain types
Normally, I like to have explicit domain types and then protect them from other layers as aggressively as possible (using a ton of duplicate types in multiple layers and mappers so that we can evolve the domain independent of infrastructure)

However, in this case, since the Events ARE the domain I've put the *most* of the domain types in the Schema so the domain is tightly coupled to the event schema. I had the thought that in an event sourced system, your schema is the real source of truth and the events are the real domain. The aggregate is just a projection of those events. Tightly coupling with the schema might be a positive in this case since the biggest risk in an event sourced system is getting out of sync with the event schema.

I don't know if this is *correct* but hey, let's try it!

TODO: I am very interested in having the Schema models be generated from some kind of evolveable schema tool like avro or protobuf. Especially if we can enforce some kind of "immutable, append only" schema.

# Testing
We are using Kotest and property tests! More info to come as this gets fleshed out.

# About running Kafka locally
Currently based on https://stackoverflow.com/questions/58004386/i-need-to-create-a-kafka-image-with-topics-already-created


Because wurstmeister kafka images allow us to define topics as environment variables.

Previously based on
https://docs.confluent.io/platform/current/quickstart/ce-docker-quickstart.html

which has a lot more going on but it doesn't seem useful for this little project (yet).