import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.4.3"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.4.30"
	kotlin("plugin.spring") version "1.4.30"
}

group = "com.acmelabs"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
	jcenter()
}


dependencies {
	// Spring and Kotlin
	implementation("org.springframework.boot:spring-boot-starter")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

	// Coroutines
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.3")

	// Kafka
	implementation("org.springframework.kafka:spring-kafka")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.kafka:spring-kafka-test")

    // web
	implementation("org.springframework.boot:spring-boot-starter-web")
	testImplementation("org.springframework.boot:spring-boot-starter-test")

	// arrow
	val arrowVersion = "0.11.0"
	implementation("io.arrow-kt:arrow-core:$arrowVersion")
	implementation("io.arrow-kt:arrow-fx:$arrowVersion")
	implementation("io.arrow-kt:arrow-optics:$arrowVersion")
	implementation("io.arrow-kt:arrow-syntax:$arrowVersion")
	implementation( "io.arrow-kt:arrow-generic:$arrowVersion")

	// Jackson serialization
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.+")

	// kotest
	testImplementation("io.kotest:kotest-runner-junit5:4.4.1")
	testImplementation("io.kotest:kotest-assertions-core:4.4.1")
	testImplementation("io.kotest:kotest-assertions-arrow-jvm:4.4.1")
	testImplementation("io.kotest:kotest-property:4.4.1")

	// EventStore
    val eventStoreVersion = "0.7"
	implementation("com.eventstore:db-client-java:$eventStoreVersion")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
